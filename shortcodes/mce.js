(function() {
	tinymce.PluginManager.add('josefine_mce_button', function( editor, url ) {
		editor.addButton( 'josefine_mce_button', {
            text: josefineLang.shortcode_josefine, 
            icon: false,
			tooltip: josefineLang.shortcode_josefine,
			type: 'menubutton',
			minWidth: 210,
			menu: [
				{
					text: josefineLang.shortcode_button,
					onclick: function() {
						editor.windowManager.open( {
							title: josefineLang.shortcode_button,
							body: [
								{
									type: 'listbox',
									name: 'ButtonType',
									label: josefineLang.shortcode_type,
									'values': [
										{text: josefineLang.shortcode_default, value: 'btn-default'},
										{text: josefineLang.shortcode_primary, value: 'btn-primary'},
										{text: josefineLang.shortcode_success, value: 'btn-success'},
										{text: josefineLang.shortcode_info, value: 'btn-info'},
										{text: josefineLang.shortcode_warning, value: 'btn-warning'},
										{text: josefineLang.shortcode_danger, value: 'btn-danger'},
									]
								},
								{
									type: 'listbox',
									name: 'ButtonSize',
									label: josefineLang.shortcode_size,
									'values': [
										{text: josefineLang.shortcode_size_large, value: 'btn-lg'},
										{text: josefineLang.shortcode_size_default, value: ''},
										{text: josefineLang.shortcode_size_small, value: 'btn-sm'},
										{text: josefineLang.shortcode_size_ex_small, value: 'btn-xs'},
									]
								},
								{
									type: 'textbox',
									name: 'ButtonText',
									label: josefineLang.shortcode_text,
									minWidth: 300,
									value: ''
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[button type="' + e.data.ButtonType + '" size="' + e.data.ButtonSize + '" text="' + e.data.ButtonText + '"]');
							}
						});
					} // onclick
				},
				{
					text: josefineLang.shortcode_progressbar,
					onclick: function() {
						editor.windowManager.open( {
							title: josefineLang.shortcode_progressbar,
							body: [
								{
									type: 'listbox',
									name: 'ProgressbarType',
									label: josefineLang.shortcode_type,
									'values': [
										{text: josefineLang.shortcode_default, value: 'default'},
										{text: josefineLang.shortcode_primary, value: 'primary'},
										{text: josefineLang.shortcode_success, value: 'success'},
										{text: josefineLang.shortcode_info, value: 'info'},
										{text: josefineLang.shortcode_warning, value: 'warning'},
										{text: josefineLang.shortcode_danger, value: 'danger'},
									]
								},
								{
									type: 'textbox',
									name: 'ProgressbarWidth',
									label: josefineLang.shortcode_width,
									minWidth: 300,
									value: '50'
								},
								{
									type: 'textbox',
									name: 'ProgressbarTitle',
									label: josefineLang.shortcode_title,
									minWidth: 300,
									value: ''
								},
								{
									type: 'listbox',
									name: 'ProgressbarStriped',
									label: josefineLang.shortcode_striped,
									'values': [
										{text: josefineLang.shortcode_true, value: 'true'},
										{text: josefineLang.shortcode_false, value: 'false'},
									]
								},
								{
									type: 'listbox',
									name: 'ProgressbarAnimation',
									label: josefineLang.shortcode_animation,
									'values': [
										{text: josefineLang.shortcode_true, value: 'true'},
										{text: josefineLang.shortcode_false, value: 'false'},
									]
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[progress_bar type="' + e.data.ProgressbarType + '" width="' + e.data.ProgressbarWidth + '" title="' + e.data.ProgressbarTitle + '" striped="' + e.data.ProgressbarStriped + '" animation="' + e.data.ProgressbarAnimation + '"]');
							}
						});
					}
				},
				{
					text: josefineLang.shortcode_status,
					onclick: function() {
						editor.windowManager.open( {
							title: josefineLang.shortcode_status,
							body: [
								{
									type: 'listbox',
									name: 'StatusType',
									label: josefineLang.shortcode_type,
									'values': [
										{text: josefineLang.shortcode_facebook, value: 'facebook'},
										{text: josefineLang.shortcode_twitter, value: 'twitter'},
										{text: josefineLang.shortcode_google_plus, value: 'google_plus'},
									]
								},
								{
									type: 'textbox',
									name: 'StatusURL',
									label: josefineLang.shortcode_url,
									minWidth: 300,
									value: ''
								},
								{
									type: 'textbox',
									name: 'StatusBackground',
									label: josefineLang.shortcode_background_img,
									minWidth: 300,
									value: ''
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[status type="' + e.data.StatusType + '" url="' + e.data.StatusURL + '" background="' + e.data.StatusBackground + '"]');
							}
						});
					}
				},
				{
					text: josefineLang.shortcode_alert,
					onclick: function() {
						editor.windowManager.open( {
							title: josefineLang.shortcode_alert,
							body: [
								{
									type: 'listbox',
									name: 'AlertType',
									label: josefineLang.shortcode_type,
									'values': [
										{text: josefineLang.shortcode_default, value: 'default'},
										{text: josefineLang.shortcode_primary, value: 'primary'},
										{text: josefineLang.shortcode_success, value: 'success'},
										{text: josefineLang.shortcode_info, value: 'info'},
										{text: josefineLang.shortcode_warning, value: 'warning'},
										{text: josefineLang.shortcode_danger, value: 'danger'},
									]
								},
								{
									type: 'listbox',
									name: 'AlertDismiss',
									label: josefineLang.shortcode_dismiss,
									'values': [
										{text: josefineLang.shortcode_true, value: 'true'},
										{text: josefineLang.shortcode_false, value: 'false'},
									]
								},
								{
									type: 'textbox',
									name: 'AlertContent',
									label: josefineLang.shortcode_content,
									value: '',									
									multiline: true,
									minWidth: 300,
									minHeight: 100
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[alert type="' + e.data.AlertType + '" dismiss="' + e.data.AlertDismiss + '"]' + e.data.AlertContent + '[/alert]');
							}
						});
					}
				},
				{
					text: josefineLang.shortcode_video,
					onclick: function() {
						editor.windowManager.open( {
							title: josefineLang.shortcode_video,
							body: [
								{
									type: 'textbox',
									name: 'VideoURL',
									label: josefineLang.shortcode_video_url,
									value: 'http://',
									minWidth: 300,
								},
								{
									type: 'textbox',
									name: 'VideoWidth',
									label: josefineLang.shortcode_width,
									value: ''
								},
								{
									type: 'textbox',
									name: 'Videoheight',
									label: josefineLang.shortcode_height,
									value: ''
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[embed width="' + e.data.VideoWidth + '" height="' + e.data.Videoheight + '"]' + e.data.VideoURL + '[/embed]');
							}
						});
					}
				},
				{
					text: josefineLang.shortcode_audio,
					onclick: function() {
						editor.windowManager.open( {
							title: josefineLang.shortcode_audio,
							body: [
								{
									type: 'textbox',
									name: 'mp3URL',
									label: josefineLang.shortcode_mp3,
									value: 'http://',
									minWidth: 300,
								},
								{
									type: 'textbox',
									name: 'm4aURL',
									label: josefineLang.shortcode_m4a,
									value: 'http://',
									minWidth: 300,
								},
								{
									type: 'textbox',
									name: 'oggURL',
									label: josefineLang.shortcode_ogg,
									value: 'http://',
									minWidth: 300,
								},
								
							],
							onsubmit: function( e ) {
								editor.insertContent( '[audio mp3="' + e.data.mp3URL + '" m4a="' + e.data.m4aURL + '" ogg="' + e.data.oggURL + '"]');
							}
						});
					}
				},
				{
					text: josefineLang.shortcode_tooltip,
					onclick: function() {
						editor.windowManager.open( {
							title: josefineLang.shortcode_tooltip,
							body: [
								{
									type: 'textbox',
									name: 'TooltipText',
									label: josefineLang.shortcode_text,
									value: '',
									minWidth: 300,
								},
								{
									type: 'listbox',
									name: 'TooltipDirection',
									label: josefineLang.shortcode_direction,
									'values': [
										{text: josefineLang.shortcode_top, value: 'top'},
										{text: josefineLang.shortcode_right, value: 'right'},
										{text: josefineLang.shortcode_bottom, value: 'bottom'},
										{text: josefineLang.shortcode_left, value: 'left'},
									]
								},
								{
									type: 'textbox',
									name: 'ToolTipContent',
									label: josefineLang.shortcode_content,
									value: '',
									multiline: true,
									minWidth: 300,
									minHeight: 100
								}
								
							],
							onsubmit: function( e ) {
								editor.insertContent( '[tooltip text="' + e.data.TooltipText + '" direction="' + e.data.TooltipDirection + '"]'+ e.data.ToolTipContent +'[/tooltip]');
							}
						});
					}
				},
				{
					text: josefineLang.shortcode_columns,
						onclick: function() {
							editor.insertContent( '\
							[row]<br />\
								[column number="6" offset=""] Column Content Goes Here [/column]<br />\
								[column number="6" offset=""] Column Content Goes Here [/column]<br />\
							[/row]');
						}
				},
				{
					text: josefineLang.shortcode_accordion,
						onclick: function() {
							editor.insertContent( '\
							[collapse_group]<br />\
								[collapse id="accordion_one" title="Accordion Title" expanded="false"] Accordion Content [/collapse]<br />\
								[collapse id="accordion_two" title="Accordion Title" expanded="false"] Accordion Content [/collapse]<br />\
							[/collapse_group]');
						}
				},
				{
					text: josefineLang.shortcode_tab,
						onclick: function() {
							editor.insertContent( '\
							[tabs]<br />\
								[tab id="tab_one" title="Tab Title" active="active"] Tab Content [/tab]<br />\
								[tab id="tab_two" title="Tab Title"] Tab Content [/tab]<br />\
								[tab id="tab_three" title="Tab Title"] Tab Content [/tab]<br />\
							[/tabs]');
						}
				},
				{
                    text: josefineLang.shortcode_custom_icon,
                    classes: 'sh-custom-icon',
                },
			]
		});
	});
})();